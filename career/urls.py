from django.urls import path
from career import views

urlpatterns = [
    path('career/', views.CareerTemplateView.as_view(), name='career')
]
