from django.views.generic import TemplateView


class CareerTemplateView(TemplateView):
    template_name = 'career/career.html'
