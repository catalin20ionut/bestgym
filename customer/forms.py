from django import forms
from django.forms import TextInput, Select
from customer.models import Customer


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'gender', 'date_of_birth', 'age', 'city', 'street', 'street_no',
                  'postal_code', 'language_of_instruction', 'phone_number', 'email', 'trainer']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please, enter the first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please, enter the last name', 'class': 'form-control'}),
            'gender': Select(attrs={'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'age': TextInput(attrs={'placeholder': 'Please, enter the age', 'class': 'form-control'}),
            'city': Select(attrs={'class': 'form-control'}),
            'street': TextInput(attrs={'placeholder': 'Please, enter the name of the street', 'class': 'form-control'}),
            'street_no': TextInput(attrs={'placeholder': 'Please, enter the number of the street',
                                          'class': 'form-control'}),
            'postal_code': TextInput(attrs={'placeholder': 'Please, enter the postal_code', 'class': 'form-control'}),
            'language_of_instruction': Select(attrs={'class': 'form-control'}),
            'phone_number': TextInput(attrs={'placeholder': 'e.g. 0754/123.456', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Please insert your email', 'class': 'form-control'}),
            'trainer': Select(attrs={'class': 'form-control'})
        }
