from rest_framework import routers
from customer import views
from django.urls import path, include
from customer.views import CustomerViewSet

router = routers.DefaultRouter()
router.register('api-customer', CustomerViewSet)

urlpatterns = [
    path('create_customer/', views.CustomerCreateView.as_view(), name='create-customer'),
    path('list_of_customers/', views.CustomerListView.as_view(), name='list-of-customers'),
    path('update_customer/<int:pk>/', views.CustomerUpdateView.as_view(), name='update-customer'),
    path('delete_customer<int:pk>/', views.CustomerDeleteView.as_view(), name='delete-customer'),
    path('detail_customer/<int:pk>/', views.CustomerDetailView.as_view(), name='detail-customer'),
    path('', include(router.urls))
]
