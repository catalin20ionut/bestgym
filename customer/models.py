from django.db import models
from trainer.models import City, Trainer


class LanguageOfInstruction(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.name}'


class Customer(models.Model):
    gender_choices = (
        ('Male', 'Masculin'),
        ('Female', 'Feminin'),
    )

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    gender = models.CharField(max_length=10, choices=gender_choices)
    date_of_birth = models.DateField()
    age = models.IntegerField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    street = models.CharField(max_length=250)
    street_no = models.IntegerField()
    postal_code = models.CharField(max_length=30, null=True, blank=True)
    phone_number = models.CharField(max_length=30)
    email = models.EmailField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    language_of_instruction = models.ForeignKey(LanguageOfInstruction, on_delete=models.CASCADE, null=True)
    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
