from django.contrib import admin
from customer.models import Customer, LanguageOfInstruction

admin.site.register(Customer)
admin.site.register(LanguageOfInstruction)
