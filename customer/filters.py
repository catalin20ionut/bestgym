import django_filters
from customer.models import Customer


class CustomerFilters(django_filters.FilterSet):
    class Meta:
        model = Customer
        fields = ['age', 'city']
