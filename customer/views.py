from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from rest_framework import viewsets

from customer.forms import CustomerForm
from customer.models import Customer
from customer.filters import CustomerFilters
from customer.serializers import CustomerSerializer


class CustomerCreateView(LoginRequiredMixin, CreateView):
    template_name = 'customer/create_customer.html'
    model = Customer
    success_url = reverse_lazy('create-customer')
    form_class = CustomerForm


class CustomerListView(LoginRequiredMixin, ListView):
    template_name = 'customer/list_customers.html'
    model = Customer
    context_object_name = 'all_customers'

    def get_context_data(self, **kwargs):
        data = super(CustomerListView, self).get_context_data(**kwargs)
        all_customers = Customer.objects.all()
        my_filter = CustomerFilters(self.request.GET, queryset=all_customers)
        all_customers = my_filter.qs
        data['all_customers'] = all_customers
        data['my_filter'] = my_filter
        return data


class CustomerUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'customer/update_customer.html'
    model = Customer
    success_url = reverse_lazy('list-of-customers')
    form_class = CustomerForm


class CustomerDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'customer/delete_customer.html'
    model = Customer
    success_url = reverse_lazy('list-of-customers')


class CustomerDetailView(LoginRequiredMixin, DetailView):
    template_name = 'customer/detail_customer.html'
    model = Customer


class CustomerViewSet(viewsets.ModelViewSet):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
