from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from subscription.forms import SubscriptionForm
from subscription.models import Subscription


class SubscriptionCreateView(LoginRequiredMixin, CreateView):
    template_name = 'subscription/create_subscription.html'
    model = Subscription
    success_url = reverse_lazy('create-subscription')
    form_class = SubscriptionForm


class SubscriptionListView(ListView):
    template_name = 'subscription/list_subscriptions.html'
    model = Subscription
    context_object_name = 'all_subscriptions'


class SubscriptionUpdateView(UpdateView):
    template_name = 'subscription/update_subscription.html'
    model = Subscription
    success_url = reverse_lazy('list-of-subscriptions')
    form_class = SubscriptionForm
