from django.db import models


class PeriodOfValidity(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.name}'


class Subscription(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now=True)
    period_of_validity = models.ForeignKey(PeriodOfValidity, on_delete=models.CASCADE, null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f'{self.name} {self.description}'
