from django import forms
from django.forms import TextInput, Select
from subscription.models import Subscription


class SubscriptionForm(forms.ModelForm):
    class Meta:
        model = Subscription
        fields = ['name', 'description', 'period_of_validity', 'price']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'name of the subscription', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'description of the subscription', 'class': 'form-control'}),
            'period_of_validity': Select(attrs={'class': 'form-control'}),
            'price': TextInput(attrs={'placeholder': 'Please, enter the price', 'class': 'form-control'})
        }
