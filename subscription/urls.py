from django.urls import path
from subscription import views

urlpatterns = [
    path('create_subscription/', views.SubscriptionCreateView.as_view(), name='create-subscription'),
    path('list_of_subscription/', views.SubscriptionListView.as_view(), name='list-of-subscriptions'),
    path('detail_subscription/<int:pk>/', views.SubscriptionUpdateView.as_view(), name='update-subscription'),
]
