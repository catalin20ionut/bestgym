from django.contrib import admin
from subscription.models import Subscription, PeriodOfValidity

admin.site.register(Subscription)
admin.site.register(PeriodOfValidity)
