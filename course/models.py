from django.db import models
from trainer.models import Trainer, City


class Course(models.Model):
    name = models.CharField(max_length=30)
    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE, default=False, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, default=False, null=True, blank=True)
    active = models.BooleanField(default=True)
    image = models.ImageField(upload_to='static/classes', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name}'
