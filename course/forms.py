from django import forms
from django.forms import Select
from django.forms import TextInput
from course.models import Course


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'trainer', 'city', 'image']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please, enter name of the course', 'class': 'form-control'}),
            'trainer': Select(attrs={'class': 'form-control'}),
            'city': Select(attrs={'class': 'form-control'}),
        }
