from django.urls import path
from course import views

urlpatterns = [
    path('create_course/', views.CourseCreateView.as_view(), name='create-course'),
    path('list_of_courses/', views.CourseListView.as_view(), name='list-of-courses'),
    path('update_course/<int:pk>/', views.CourseUpdateView.as_view(), name='update-course'),
    path('detail_course/<int:pk>/', views.CourseDetailView.as_view(), name='detail-course'),
]
