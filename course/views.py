from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DetailView
from course.forms import CourseForm
from course.models import Course


class CourseCreateView(CreateView):
    template_name = 'course/create_course.html'
    model = Course
    success_url = reverse_lazy('create-course')
    form_class = CourseForm


class CourseListView(ListView):
    template_name = 'course/list_courses.html'
    model = Course
    context_object_name = 'all_courses'


class CourseUpdateView(UpdateView):
    template_name = 'course/update_course.html'
    model = Course
    success_url = reverse_lazy('list-of-trainers')
    form_class = CourseForm


class CourseDetailView(DetailView):
    template_name = 'course/detail_course.html'
    model = Course
