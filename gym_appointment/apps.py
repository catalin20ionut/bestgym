from django.apps import AppConfig


class GymAppointmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gym_appointment'
