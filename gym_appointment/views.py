from django.views.generic import TemplateView


class HomeTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage.html'


class HomePageTwoTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage_2.html'


class HomePageThreeTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage_3.html'


class HomePageFourTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage_4.html'


class HomePageFiveTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage_5.html'


class HomePageSixTemplateView(TemplateView):
    template_name = 'gym_appointment/homepage_6.html'
