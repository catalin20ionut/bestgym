from django.urls import path
from gym_appointment import views

urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='homepage'),
    path('homepage_2/', views.HomePageTwoTemplateView.as_view(), name='homepage_2'),
    path('homepage_3/', views.HomePageThreeTemplateView.as_view(), name='homepage_3'),
    path('homepage_4/', views.HomePageFourTemplateView.as_view(), name='homepage_4'),
    path('homepage_5/', views.HomePageFiveTemplateView.as_view(), name='homepage_5'),
    path('homepage_6/', views.HomePageSixTemplateView.as_view(), name='homepage_6'),
]
