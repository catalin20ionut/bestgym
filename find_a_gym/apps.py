from django.apps import AppConfig


class FindAGymConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'find_a_gym'
