from django.urls import path
from find_a_gym import views

urlpatterns = [
    path('find_a_gym/', views.FindAGymTemplateView.as_view(), name='find-a-gym')
]
