from django.views.generic import TemplateView


class FindAGymTemplateView(TemplateView):
    template_name = 'find_a_gym/find_a_gym.html'
