import django_filters
from trainer.models import Trainer


class TrainerFilters(django_filters.FilterSet):
    class Meta:
        model = Trainer
        fields = ['city']
