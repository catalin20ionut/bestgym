from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView
from rest_framework import viewsets

from course.models import Course
from customer.models import Customer
from trainer.models import Trainer
from trainer.forms import TrainerForm
from trainer.filters import TrainerFilters
from trainer.serializers import TrainerSerializer


class TrainerCreateView(LoginRequiredMixin, CreateView):
    template_name = 'trainer/create_trainer.html'
    model = Trainer
    success_url = reverse_lazy('create-trainer')
    form_class = TrainerForm


class TrainerListView(ListView):
    template_name = 'trainer/list_trainers.html'
    model = Trainer
    context_object_name = 'all_trainers'

    def get_context_data(self, **kwargs):
        data = super(TrainerListView, self).get_context_data(**kwargs)
        all_trainers = Trainer.objects.all()
        my_filter = TrainerFilters(self.request.GET, queryset=all_trainers)
        all_trainers = my_filter.qs
        data['all_trainers'] = all_trainers
        data['my_filter'] = my_filter
        return data


class TrainerUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'trainer/update_trainer.html'
    model = Trainer
    success_url = reverse_lazy('list-of-trainers')
    form_class = TrainerForm


def get_all_customers_per_trainer(request, id_trainer):
    all_customers_per_trainer = Customer.objects.filter(trainer_id=id_trainer)
    context = {'customers': all_customers_per_trainer}
    return render(request, 'customer/get_all_customers_per_trainer.html', context)


def get_all_courses_per_trainer(request, id_trainer):
    all_courses_per_trainer = Course.objects.filter(trainer_id=id_trainer)
    context = {'courses': all_courses_per_trainer}
    return render(request, 'course/get_all_courses_per_trainer.html', context)


class TrainerViewSet(viewsets.ModelViewSet):
    queryset = Trainer.objects.all()
    serializer_class = TrainerSerializer
