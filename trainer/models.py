from django.db import models


class City(models.Model):
    city = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.city}'


class Street(models.Model):
    street = models.CharField(max_length=250)

    def __str__(self):
        return f'{self.street}'


class Trainer(models.Model):
    gender_choices = (
        ('Male', 'Masculin'),
        ('Female', 'Feminin'),
    )

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    local_manager = models.BooleanField(default=False)
    gender = models.CharField(max_length=10, choices=gender_choices)
    date_of_birth = models.DateField()
    age = models.IntegerField()
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    street = models.ForeignKey(Street, on_delete=models.CASCADE)
    street_no = models.IntegerField()
    romanian = models.BooleanField(default=False)
    english = models.BooleanField(default=False)
    german = models.BooleanField(default=False)
    hungarian = models.BooleanField(default=False)
    image = models.ImageField(upload_to='static/trainers', null=True, blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
