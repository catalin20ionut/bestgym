from django.contrib import admin
from trainer.models import Trainer, City, Street

admin.site.register(Trainer)
admin.site.register(City)
admin.site.register(Street)
