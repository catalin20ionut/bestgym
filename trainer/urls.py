from django.urls import path, include
from rest_framework import routers
from trainer import views
from trainer.views import TrainerViewSet

router = routers.DefaultRouter()
router.register('api-trainer', TrainerViewSet)

urlpatterns = [
    path('create_trainer/', views.TrainerCreateView.as_view(), name='create-trainer'),
    path('list_of_trainers/', views.TrainerListView.as_view(), name='list-of-trainers'),
    path('update_trainer/<int:pk>/', views.TrainerUpdateView.as_view(), name='update-trainer'),
    path('customers_per_trainer/<int:id_trainer>/', views.get_all_customers_per_trainer, name='customers-per-trainer'),
    path('courses_per_trainer/<int:id_trainer>/', views.get_all_courses_per_trainer, name='courses-per-trainer'),
    path('', include(router.urls))
]
