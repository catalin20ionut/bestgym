from django import forms
from django.forms import TextInput, Select
from trainer.models import Trainer


class TrainerForm(forms.ModelForm):
    class Meta:
        model = Trainer
        fields = ['first_name', 'last_name', 'local_manager', 'gender', 'date_of_birth', 'age', 'city', 'street',
                  'street_no', 'romanian', 'english', 'german', 'hungarian', 'active', 'image']
        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please, enter the first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please, enter the last name', 'class': 'form-control'}),
            'gender': Select(attrs={'class': 'form-control'}),
            'date_of_birth': TextInput(attrs={'class': 'form-control', 'type': 'date'}),
            'age': TextInput(attrs={'placeholder': 'Please, enter the age', 'class': 'form-control'}),
            'city': Select(attrs={'class': 'form-control'}),
            'street': Select(attrs={'class': 'form-control'}),
            'street_no': TextInput(attrs={'placeholder': 'Please, enter the number of the street',
                                          'class': 'form-control'}),
        }
