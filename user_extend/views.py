from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import CreateView
from user_extend.forms import UserExtendForm


class CreateUser(CreateView):
    template_name = 'user_extend/create_user.html'
    model = User
    success_url = reverse_lazy('create-user')
    form_class = UserExtendForm
