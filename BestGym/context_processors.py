from trainer.models import Trainer


def get_all_trainers(request):
    all_trainers = Trainer.objects.all()
    return {'trainers': all_trainers}
