from django.views.generic import TemplateView


class ScheduleTemplateView(TemplateView):
    template_name = 'schedule/schedule.html'


class GoogleMapTemplateView:
    pass