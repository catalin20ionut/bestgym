from django.urls import path
from schedule import views

urlpatterns = [
    path('timetable/', views.ScheduleTemplateView.as_view(), name='timetable')
]
